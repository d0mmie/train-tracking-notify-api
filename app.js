import axios from 'axios'
import bodyParser from 'body-parser'
import chalk from 'chalk'
import express from 'express'
import _ from 'lodash'
import moment from 'moment'
import qs from 'qs'
import Sequelize, { STRING, DATE, INTEGER } from 'sequelize'

import station from './station.json'

moment.locale('th')

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER_NAME, process.env.DB_PASSWORD, {
  host: 'localhost',
  dialect: 'mariadb'
})

const app = express()
const port = process.env.NODE_ENV !== 'production' ? (8081) : parseInt(process.env.PORT)

console.log(port)

const TransactionModel = sequelize.define('transactions', {
  id: {
    type: STRING(36),
    primaryKey: true
  },
  trains_no: {
    type: STRING(3),
    allowNull: false
  },
  station_code: {
    type: STRING(4),
    allowNull: false
  },
  fetch_time: {
    type: DATE,
    allowNull: false
  },
  run_date: {
    type: DATE,
    allowNull: false
  },
  arrive_time: {
    type: DATE,
    allowNull: false
  },
  departure_time: {
    type: DATE,
    allowNull: false
  },
  departure_late: {
    type: INTEGER,
    allowNull: false
  },
  ref_time: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
  },
  createdAt: {
    type: DATE
  }
})

const SubscriptionModel = sequelize.define('subscriptions', {
  id: {
    type: STRING(34),
    primaryKey: true
  },
  trains_no: {
    type: STRING(3),
    allowNull: false
  },
  station_code: {
    type: STRING(4),
    allowNull: false
  },
  station_before_code: {
    type: STRING(4),
    allowNull: false
  },
  line_notify_token: {
    type: STRING(255),
    allowNull: false
  }
}, {
    timestamps: false
  })

const fetchTrainByNo = no => {
  return axios.post('http://tts.railway.co.th/ttsmaster/checktrain.php', qs.stringify({ grant: 'user', train: no }), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    }
  })
}

const httpNotify = (message, token) => {
  axios.post('https://notify-api.line.me/api/notify', qs.stringify({ message }), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': `Bearer ${token}`
    }
  }).catch((e) => {
    console.log(`[${moment().toJSON()}] notify request error.`)
  })
}

const notify = async (token, station_code, trains_no, late, before, expected_time, destination_station_code) => {
  if (before) {
    const message = `${moment().format('DD-MM-YY@HH:mm').toString()} รถไฟขบวน ${trains_no} : ถึงสถานี ${_.find(station, { station_code }).station_name} ช้าไป ${late} นาที คาดว่าจะถึง ${_.find(station, { station_code: destination_station_code }).station_name} ${moment().format('DD-MM-YY').toString()}@${expected_time}`
    httpNotify(message, token)
  } else {
    const message = `${moment().format('DD-MM-YY@HH:mm').toString()} รถไฟขบวน ${trains_no} : ถึงสถานี ${_.find(station, { station_code }).station_name} ช้าไป ${late} นาที`
    httpNotify(message, token)
  }
}

const notifyHandle = async (data) => {
  const matchTrainArrive = _.map(data, ({ trains_no, station_code }) => SubscriptionModel.findAll({
    where: {
      trains_no,
      station_code
    }
  }))

  const matchTrainArriveResult = await Promise.all(matchTrainArrive)
  const flattedMappedMatchArriveResult = _.flatten(matchTrainArriveResult).map(({ dataValues: { line_notify_token, trains_no, station_code } }) => {
    return fetchTrainByNo(trains_no).then((response) => {
      notify(line_notify_token, station_code, trains_no, response.data.left.latestdelay)
    })
  })

  const beforeTrainArrive = _.map(data, ({ trains_no, station_code }) => SubscriptionModel.findAll({
    where: {
      trains_no,
      station_before_code: station_code
    }
  }))

  const beforeTrainArriveResult = await Promise.all(beforeTrainArrive)
  const flattedMappedBeforeTrainArriveResult = _.flatten(beforeTrainArriveResult).map(({ dataValues: { line_notify_token, trains_no, station_before_code, station_code } }) => {
    return fetchTrainByNo(trains_no).then((response) => {
      notify(line_notify_token, station_before_code, trains_no, response.data.left.latestdelay, true, _.find(response.data.right, { stcode: station_code }).actarr, station_code)
    })
  })

  const resultPromiseAll = await Promise.all([...flattedMappedMatchArriveResult, ...flattedMappedBeforeTrainArriveResult])
  await Promise.all(resultPromiseAll)
}

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.post('/v1/addTransaction', (req, res) => {
  try {
    if (req.headers.authorization.split(' ')[1] !== process.env.TOKEN) {
      res.status(403).end('Authorization error.')
    } else {
      notifyHandle(req.body.data)
      const promisePool = _.map(req.body.data, o => TransactionModel.create(o, { fields: ['trains_no', 'station_code', 'fetch_time', 'run_date', 'arrive_time', 'departure_time', 'ref_time', 'departure_late'] }))
      Promise.all(promisePool).then(() => {
        res.status(200).end('success')
      }).catch(() => {
        console.log(chalk.red(`[${moment().toJSON()}] insert transaction to database error`))
      })
    }
  } catch (e) {
    res.end(err)
  }
})

sequelize.authenticate()
  .then(() => {
    console.log(chalk.green(`[${moment().toJSON()}] Connection has been established successfully.`))
    app.listen(port, () => {
      console.log(`[${moment().toJSON()}] App started on port ${port}`)
    })
  })
  .catch(err => {
    console.error(`[${moment().toJSON()}] Unable to connect to the database: `, err)
  })
